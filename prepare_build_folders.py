import os
import subprocess
from pathlib import Path

root_dir = Path("/home/jacques/blender")
blender_source_dir = root_dir / "blender"
build_root_dir = root_dir / "builds"

build_debug_dir = build_root_dir / "debug_gcc"
build_release_dir = build_root_dir / "release_gcc"
build_reference_gcc_dir = build_root_dir / "reference_release_gcc"
build_compile_commands_dir = build_root_dir / "compile_commands"
build_clang_release_dir = build_root_dir / "clang_release"
build_clang_debug_dir = build_root_dir / "clang_debug"
build_asan_debug_dir = build_root_dir / "asan_debug_gcc"

build_all = True
all_build_dirs = [
    build_debug_dir,
    build_release_dir,
    build_reference_gcc_dir,
    build_compile_commands_dir,
    build_clang_release_dir,
    build_clang_debug_dir,
    build_asan_debug_dir,
]

common_cmake_flags = [
    "-GNinja",
    "-DWITH_BUILDINFO=OFF",
    "-DWITH_GTESTS=ON",
    "-DCMAKE_CXX_FLAGS_DEBUG=-g",
    "-DCMAKE_C_FLAGS_DEBUG=-g",
    "-DCMAKE_EXPORT_COMPILE_COMMANDS=ON",
    "-DWITH_NINJA_POOL_JOBS=ON",
    "-DWITH_CYCLES_NATIVE_ONLY=ON",
    "-DWITH_LIBMV_SCHUR_SPECIALIZATIONS=OFF",
    "-DWITH_COMPILER_CCACHE=ON",
    "-DWITH_COMPILER_CODE_COVERAGE=ON",
]

linker_mold = ["-DWITH_LINKER_MOLD=ON"]
linker_lld = ["-DWITH_LINKER_LLD=ON", "-DWITH_LINKER_GOLD=OFF"]

common_gcc_flags = [
    "-DCMAKE_CXX_FLAGS=-Wno-stringop-overflow -Wno-stringop-overread -fdiagnostics-color=always -ffold-simple-inlines",
    "-DCMAKE_C_FLAGS=-Wno-stringop-overflow -Wno-stringop-overread -fdiagnostics-color=always",
]

common_clang_flags = [
    "-DCMAKE_C_FLAGS=-fdiagnostics-color=always",
    "-DCMAKE_CXX_FLAGS=-fdiagnostics-color=always",
    "-DWITH_CYCLES=OFF",
    "-DWITH_OPENVDB=OFF",
    "-DWITH_USD=OFF",
]

common_asan_flags = [
    "-DWITH_COMPILER_ASAN=ON",
    "-DWITH_MEM_JEMALLOC=OFF",
    "-DWITH_MEM_VALGRIND=OFF",
    "-DWITH_NINJA_POOL_JOBS=ON",
]

build_type_debug = "-DCMAKE_BUILD_TYPE=Debug"
build_type_release = "-DCMAKE_BUILD_TYPE=Release"

clang_env = {"CC": "clang", "CXX": "clang++"}

build_root_dir.mkdir(exist_ok=True)

if not build_debug_dir.exists():
    args = [
        "cmake",
        str(blender_source_dir),
        build_type_debug,
        *linker_mold,
        *common_cmake_flags,
        *common_gcc_flags,
    ]
    print(" ".join(args))
    build_debug_dir.mkdir()
    subprocess.run(args, cwd=build_debug_dir, check=True)

if not build_release_dir.exists():
    args = [
        "cmake",
        str(blender_source_dir),
        build_type_release,
        *linker_mold,
        *common_cmake_flags,
        *common_gcc_flags,
    ]
    print(" ".join(args))
    build_release_dir.mkdir()
    subprocess.run(args, cwd=build_release_dir, check=True)

if not build_reference_gcc_dir.exists():
    args = [
        "cmake",
        str(blender_source_dir),
        build_type_release,
        *linker_mold,
        *common_cmake_flags,
        *common_gcc_flags,
    ]
    print(" ".join(args))
    build_reference_gcc_dir.mkdir()
    subprocess.run(args, cwd=build_reference_gcc_dir, check=True)

if not build_compile_commands_dir.exists():
    args = [
        "cmake",
        str(blender_source_dir),
        build_type_debug,
        *linker_mold,
        *common_cmake_flags,
        "-DWITH_UNITY_BUILD=OFF",
    ]
    print(" ".join(args))
    build_compile_commands_dir.mkdir()
    subprocess.run(args, cwd=build_compile_commands_dir, check=True)

if not build_clang_release_dir.exists():
    args = [
        "cmake",
        str(blender_source_dir),
        build_type_release,
        *linker_mold,
        *common_cmake_flags,
        *common_clang_flags,
    ]
    print(" ".join(args))
    build_clang_release_dir.mkdir()
    subprocess.run(
        args, cwd=build_clang_release_dir, env={**clang_env, **os.environ}, check=True
    )

if not build_clang_debug_dir.exists():
    args = [
        "cmake",
        str(blender_source_dir),
        build_type_debug,
        *linker_mold,
        *common_cmake_flags,
        *common_clang_flags,
    ]
    print(" ".join(args))
    build_clang_debug_dir.mkdir()
    subprocess.run(
        args, cwd=build_clang_debug_dir, env={**clang_env, **os.environ}, check=True
    )

if not build_clang_release_dir.exists():
    args = [
        "cmake",
        str(blender_source_dir),
        build_type_release,
        *linker_mold,
        *common_cmake_flags,
        *common_clang_flags,
    ]
    print(" ".join(args))
    build_clang_release_dir.mkdir()
    subprocess.run(
        args, cwd=build_clang_release_dir, env={**clang_env, **os.environ}, check=True
    )

# This is not yet working for some reason.
if not build_asan_debug_dir.exists():
    args = [
        "cmake",
        str(blender_source_dir),
        build_type_debug,
        *linker_lld,
        *common_cmake_flags,
        *common_clang_flags,
        *common_asan_flags,
    ]
    print(" ".join(args))
    build_asan_debug_dir.mkdir()
    subprocess.run(
        args, cwd=build_asan_debug_dir, env={**clang_env, **os.environ}, check=True
    )

if build_all:
    for build_dir in all_build_dirs:
        if not build_dir.exists():
            continue
        print(f"Building {build_dir}")
        if "compile_commands" in build_dir.name:
            subprocess.run(
                [
                    "ninja",
                    "makesrna",
                    "bf_rna",
                    "source/blender/makesdna/intern/dna_type_offsets.h",
                ],
                cwd=build_dir,
                check=True,
            )
        else:
            subprocess.run(["ninja", "install"], cwd=build_dir, check=True)
