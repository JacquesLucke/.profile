#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2023 Blender Authors
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""
Gathers information that a developer might want to include in a weekly report.
"""

import argparse
import datetime
import json
import sys
from pathlib import Path

USER_NAME = "jacqueslucke"
BLENDER_SOURCE_PATH = Path("/home/jacques/blender/blender")

sys.path.append(str(BLENDER_SOURCE_PATH / "tools" / "triage"))

from gitea_utils import gitea_json_activities_get, BASE_API_URL, url_json_get

def main():
    args = argparse_create().parse_args()
    username = args.username

    now = datetime.datetime.now()
    monday = datetime.datetime(now.year, now.month, now.day) - datetime.timedelta(days=now.weekday())

    while True:
        if input(f"Generate for {week_to_weekly_title(monday)}? [y]/n: ") in ("", "y"):
            break
        monday -= datetime.timedelta(weeks=1)

    commit_lines = set()
    touched_pull_request_ids = set()

    for weekday_index in range(7):
        day = monday + datetime.timedelta(days=weekday_index)
        day_str = day.strftime("%Y-%m-%d")
        for activity in gitea_json_activities_get(username, day_str):
            op_type = activity["op_type"]
            if op_type == "commit_repo":
                if not activity["content"]:
                    continue
                content_json = json.loads(activity["content"])
                repo_fullname = activity["repo"]["full_name"]
                if activity["repo"]["name"] == "blender" and activity["repo"]["full_name"] != "blender/blender":
                    continue
                if activity["ref_name"] != "refs/heads/main" and not activity["ref_name"].startswith("refs/heads/blender-v"):
                    continue
                if ".profile" in repo_fullname:
                    continue
                for commit in content_json["Commits"]:
                    if commit["AuthorName"] != activity["act_user"]["full_name"]:
                        continue
                    commit_hash = commit["Sha1"][:10]
                    extra_prefix = {"blender/blender-developer-docs" : "Docs: "}.get(repo_fullname, "")
                    title = commit["Message"].split("\n", 1)[0]
                    if "Merge branch" in title:
                        continue
                    commit_url = f"https://projects.blender.org/{repo_fullname}/commit/{commit_hash}"
                    commit_data = f"{extra_prefix}{title} ([{commit_hash[:7]}]({commit_url}))"
                    commit_lines.add(commit_data)
            elif op_type in ("comment_pull", "create_pull_request", "merge_pull_request"):
                pull_request_id = activity["content"].split("|")[0]
                repo_fullname = activity["repo"]["full_name"]
                touched_pull_request_ids.add((repo_fullname, pull_request_id))

    pr_lines = []

    for repo_fullname, pull_request_id in touched_pull_request_ids:
        pr_data = gitea_json_pull_request_get(repo_fullname, pull_request_id)
        if pr_data["user"]["username"].lower() != username.lower():
            continue
        # TODO: Check close reason and time to avoid missing some PRs.
        if pr_data["state"] != "open":
            continue
        pr_lines.append(f"[PR #{pull_request_id}]({pr_data["html_url"]}): {pr_data["title"]}")

    commit_lines = list(sorted(commit_lines, key=lambda x: (sort_commit_priority(x), x.lower())))
    pr_lines = list(sorted(pr_lines, key=lambda x: (sort_pull_request_priority(x), x.lower())))

    for line in commit_lines:
        print(f"* {line}")
    for line in pr_lines:
        print(f"* {line}")

def sort_commit_priority(line):
    line = line.lower()
    if line.startswith("fix #"):
        return 0
    if line.startswith("fix"):
        return 1
    if line.startswith("cleanup"):
        return 3
    if "blender-developer-docs" in line:
        return 4
    if ":" in line:
        return 2
    return 4

def sort_pull_request_priority(line):
    line = line.lower()
    if "wip:" in line:
        return 1
    return 0

def week_to_weekly_title(monday: datetime.datetime):
    if monday.weekday() != 0:
        raise RuntimeError("Expected a monday as beginning of week")
    sunday = monday + datetime.timedelta(days=6)
    begin_date_str = monday.strftime('%B ') + str(monday.day)
    end_date_str = str(sunday.day) if monday.month == sunday.month else sunday.strftime('%B ') + str(sunday.day)
    return f"{begin_date_str} - {end_date_str}"

def gitea_json_pull_request_get(repo_fullname, id):
    """
    Get pull request JSON data.
    :param repo_fullname: string in the format "{owner}/{repo}"
    :param id: number of the pull request, without # prefix.
    """
    url = f"{BASE_API_URL}/repos/{repo_fullname}/pulls/{id}"
    return url_json_get(url)

def argparse_create():
    parser = argparse.ArgumentParser(description="Generate Dev Weekly Report")

    parser.add_argument(
        "--username",
        dest="username",
        type=str,
        default=USER_NAME,
        required=False)

    return parser



if __name__ == "__main__":
    main()
