<table>
    <tr><td><strong>blender.chat</strong></td><td><a href='https://blender.chat/direct/JacquesLucke'>JacquesLucke</a></td></tr>
    <tr><td><strong>Email</strong></td><td>jacques@blender.org</td></tr>
    <tr><td><strong>Mastodon</strong></td><td><a href='https://mstdn.social/@JacquesLucke'>@JacquesLucke@mstdn.social</a></td></tr>
    <tr><td><strong>Twitter/X</strong></td><td><a href='https://twitter.com/JacquesLucke'>@JacquesLucke</a></td></tr>
    <tr><td><strong>Weekly Reports</strong></td><td>
<a href='https://projects.blender.org/JacquesLucke/.profile/src/branch/main/reports/2025.md'>2025</a>
/ <a href='https://projects.blender.org/JacquesLucke/.profile/src/branch/main/reports/2024.md'>2024</a>
/ <a href='https://wiki.blender.org/wiki/User:JacquesLucke/Reports/2023'>2023</a>
/ <a href='https://wiki.blender.org/wiki/User:JacquesLucke/Reports/2022'>2022</a>
/ <a href='https://wiki.blender.org/wiki/User:JacquesLucke/Reports/2021'>2021</a>
/ <a href='https://wiki.blender.org/wiki/User:JacquesLucke/Reports/2020'>2020</a>
/ <a href='https://wiki.blender.org/wiki/User:JacquesLucke/Reports/2019'>2019</a>
/ <a href='https://wiki.blender.org/wiki/User:JacquesLucke/Reports/2018'>2018</a>
    </td></tr>
</table>

I'm a Blender developer, employed by the Blender Institute. I'm mainly working in the [Nodes & Physics module](https://projects.blender.org/blender/blender/wiki/Module:%20Nodes%20&%20Physics).